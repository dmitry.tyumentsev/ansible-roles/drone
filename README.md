Drone role
=========

Installs and configures [Drone CI](https://www.drone.io/) server on Fedora or Debian/Ubuntu servers.

Dependencies
------------

`tyumentsev4.docker` role to install docker engine.

Example Playbook
----------------
```yml
- hosts: drone
  become: true
  roles:
    - tyumentsev4.drone
```
